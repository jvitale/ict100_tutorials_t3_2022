const gameState = {
    deck: null,
    playerWallet: 100,
    currentBet: null,
    cards: [[], []],
    hiddenCard: null
}

class Card{
    constructor(face, suit){
        this.face = face;
        this.suit = suit;
        this.visible = true;
    }

    setCardVisibility(visibility){
        this.visible = visibility;
    }

    getValue(){
        if (this.visible === true){
            if (!isNaN(Number(this.face))){
                return Number(this.face);
            } else if (this.face === 'ace'){
                return 11;
            } else {
                return 10;
            }
        } else {
            return 0;
        }
    }

    draw(){
        if (this.visible){
            return `<img style="background-color: white; width: 70px;" src="cards/${this.face}_of_${this.suit}.png" />`;
        } else {
            return `<img style="background-color: white; width: 70px;" src="cards/back.png" />`
        }
    }
}

class Deck{
    constructor(){
        this.buildDeck();
        this.shuffle();
    }

    buildDeck(){
        this.deck = [];
        let suits = ['hearts', 'clubs', 'spades', 'diamonds'];
        let faces = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'jack', 'queen', 'king', 'ace'];
        for (let i = 0; i < suits.length; i++){
            for (let j = 0; j < faces.length; j++){
                this.deck.push(new Card(faces[j], suits[i]));
            }
        }
    }

    shuffle(){
        /* Randomize array in-place using Durstenfeld shuffle algorithm */
        for (let i = this.deck.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = this.deck[i];
            this.deck[i] = this.deck[j];
            this.deck[j] = temp;
        }
    }

    dealCard(){
        let newCard = this.deck.pop();
        if (this.deck.length <= 26){
            this.buildDeck();
            this.shuffle();
        }
        return newCard;
    }
}

function collectBet(){
    let currentBet;
    let isValidBet = false;
    //let counter = 0;
    do {
        console.log("please provide your bet");
        let inputBetElement = document.getElementById('player-bet-input');
        currentBet = Number(inputBetElement.value); 
        if (isNaN(currentBet) || currentBet <= 0 || currentBet > gameState.playerWallet){
            console.log('Your bet is not valid!');
            //counter++;
        } else {
            isValidBet = true;
        }
    } while(!isValidBet) //&& counter < 4);

    return currentBet;
}

function drawCard(){
    let min = 2;
    let max = 11;
    let rndNum = min + Math.floor(Math.random()*(max - min + 1 ));

    return rndNum;
}

function handValue(cardsObj){
    let sum = 0;
    let cards = [];
    for (let i = 0; i < cardsObj.length; i++){
        cards.push(cardsObj[i].getValue());
    }
    if (cards.length > 0) {
        sum = cards.reduce(
            (accumulator, item) => accumulator + item
        );
        let aces = cards.filter(
            (item) => item === 11
        ).length;
        /*
        for (let i = 0; i < cards.length; i++){
            sum = sum + cards[i];
            if (cards[i] == 11){
                aces++;
            } 
        }
        */
        while (sum > 21 && aces > 0){
            sum = sum - 10;
            aces--;
        }
    }
    return sum;
}

function resetGameState(){
    gameState.deck = new Deck();
    gameState.currentBet = null;
    gameState.cards = [[], []];
    gameState.hiddenCard = null;
}

function updateGUI(){
    let walletElement = document.getElementById('player-wallet');
    walletElement.innerHTML = gameState.playerWallet;
    let ids = ['player-cards', 'dealer-cards'];
    let scoreIds = ['player-score', 'dealer-score'];
    for (let k = 0; k < ids.length; k++){
        // clean all the cells of player's cards and dealer's cards
        let table = document.getElementById(ids[k]);
        let cells = table.getElementsByTagName('td');
        for (let i = 0; i < cells.length; i++){
            cells[i].innerHTML = '&nbsp;';
        }
        // set values of player's cards and dealer's cards in each cell
        for (let i = 0; i < gameState.cards[k].length; i++){
            cells[i].innerHTML = gameState.cards[k][i].draw();
        }
        let curScore = handValue(gameState.cards[k]);
        let scoreElement = document.getElementById(scoreIds[k]);
        scoreElement.innerHTML = curScore;
    }
}

function newGame(){
    resetGameState();
    updateGUI();
    log('&nbsp;');
    let inputBetElement = document.getElementById('player-bet-input');
    inputBetElement.disabled = false;
    let buttonBetElement = document.getElementById('player-bet-button');
    buttonBetElement.disabled = false;
    let walletCellElement = document.getElementById('player-wallet');
    walletCellElement.innerHTML = gameState.playerWallet;

    let buttonRunElement = document.getElementById('button-run');
    buttonRunElement.disabled = true;
}

function collectBetAndDealCards(){
    gameState.currentBet = collectBet();
    gameState.playerWallet -= gameState.currentBet;

    let inputBetElement = document.getElementById('player-bet-input');
    inputBetElement.disabled = true;
    let buttonBetElement = document.getElementById('player-bet-button');
    buttonBetElement.disabled = true;

    // we deal the first 2 cards for the player
    // we deal the first 2 cards for the dealer
    // one is hidden
    for(let playerIndex = 0; playerIndex < gameState.cards.length; playerIndex++){
        for(let cardIndex = 0; cardIndex < 2; cardIndex++){
            let newCard = gameState.deck.dealCard();
            if (cardIndex === 1 && playerIndex === 1){
                newCard.setCardVisibility(false);
            }
            gameState.cards[playerIndex][cardIndex] = newCard;
        }
    }

    updateGUI();

    let curPlayerScore = handValue(gameState.cards[0]);

    // check immediately that the player has blackjack
    // if so, we need to pay 2.5 the bet -> end the game
    if (curPlayerScore === 21){
        log('Player has blackjack!');
        payBet(2.5);
    }

    let buttonStay = document.getElementById('button-stay');
    buttonStay.disabled = false;
    let buttonHit = document.getElementById('button-hit');
    buttonHit.disabled = false;
}

function log(message){
    let logElement = document.getElementById('console-log');
    logElement.innerHTML = message;
}

function hit(){
    let curCard = gameState.deck.dealCard();
    gameState.cards[0].push(curCard);
    let curPlayerScore = handValue(gameState.cards[0]);
    updateGUI()
    if (curPlayerScore > 21){
        log('Player busted!')
        payBet(0);  
    }
}

function stay(){
    let buttonStay = document.getElementById('button-stay');
    buttonStay.disabled = true;
    let buttonHit = document.getElementById('button-hit');
    buttonHit.disabled = true;

    for (let i = 0; i < gameState.cards[1].length; i++){
        gameState.cards[1][i].setCardVisibility(true);
    }

    let curPlayerScore = handValue(gameState.cards[0]);
    let curDealerScore = handValue(gameState.cards[1]);

    updateGUI();

    // REPEAT THIS -----
    // If dealer has less than 17
    // dealer draws a card
    // --- UNTIL THE DEALER has 17 or more
    while(curDealerScore < 17){
        let curCard = gameState.deck.dealCard();
        console.log(`The dealer got a new card with value ${curCard}`);
        gameState.cards[1].push(curCard);
        curDealerScore = handValue(gameState.cards[1]);
        console.log(`The dealer got a score of ${curDealerScore}`);
        updateGUI();
    }
    // if new score > 21 dealer loses -> 
    //    need to pay the player 2 times the bet 
    //    -> end the game
    if (curDealerScore > 21){
        log(`Dealer has busted. The player wins.`);
        payBet(2);
    }

    // we need to compare player's score with dealer's score
    // if player's score > dealer's score: player wins
    //    -> pay 2 times the bet
    // end the game
    if (curDealerScore > curPlayerScore){
        log(`The player lost`);
        payBet(0);
    } else if (curDealerScore === curPlayerScore){
        log(`That's a tie`);
        payBet(1);
    } else {
        log(`The player wins`);
        payBet(2);
    }

     
}

function payBet(times){
    gameState.playerWallet += gameState.currentBet*times;
    updateGUI();

    let buttonBetElement = document.getElementById('player-bet-button');
    buttonBetElement.disabled = true;

    let buttonStay = document.getElementById('button-stay');
    buttonStay.disabled = true;
    let buttonHit = document.getElementById('button-hit');
    buttonHit.disabled = true;

    let buttonRunElement = document.getElementById('button-run');
    buttonRunElement.disabled = false;
}

function addEventsListeners(){
    let buttonRunElement = document.getElementById('button-run');

    buttonRunElement.addEventListener(
        'click',
        (event) => {
            newGame();
        }
    );

    let buttonBetElement = document.getElementById('player-bet-button');
    buttonBetElement.addEventListener(
        'click',
        (event) => {
            collectBetAndDealCards();
        }
    );

    let buttonStay = document.getElementById('button-stay');
    buttonStay.addEventListener(
        'click',
        (event) => {
            stay();
        }
    );

    let buttonHit = document.getElementById('button-hit');
    buttonHit.addEventListener(
        'click',
        (event) => {
            hit();
        }
    );
}

